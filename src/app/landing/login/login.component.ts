import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { AdminService } from 'src/app/services/admin.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private toast: ToastrService,
              private spinner: NgxSpinnerService,
              private router: Router,
              private service: AdminService) { }

  ngOnInit(): void {
  }

  loginCtl(phno, password) {
    let login = {phno: phno, password: password}
    if(login.phno  !== '' && login.password !==  '') {
      this.service.login(login).subscribe(res => {
        console.log(res)
        if (res.status === 'success' && res.data) {
          sessionStorage.setItem('ci_admin', res.data)
          this.toast.success('login success')
          this.router.navigate(['/dashboard'])
        } else {
          this.toast.error('Not authenticated','Error')
        }
      })
    } else {
      this.toast.warning('some fields are missing')
    }
  }

}
