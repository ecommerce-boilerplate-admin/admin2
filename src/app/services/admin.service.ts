import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AdminService {
  public api = environment.api+'admin'
  constructor(private http: HttpClient) { }

  getall():Observable<any> {
    return this.http.post(this.api+`/read`,{})
  }
  add(data):Observable<any> {
    return this.http.post(this.api+`/create/`, data)
  }
  update(id, data):Observable<any> {
    return this.http.patch(this.api+`/update/${id}`, data)
  }
  delete(id):Observable<any> {
    return this.http.delete(this.api+`/${id}`)
  }
  byid(id):Observable<any> {
    return this.http.get(this.api+`/${id}`)
  }
  bycategory(category):Observable<any> {
    return this.http.get(this.api+`/all?filter[parent_category]=${category}`)
  }
  isLogin():Observable<any>{
    return this.http.get(this.api+`/isLogin/${sessionStorage.getItem('ci_admin')}`, )
  }

  login(data):Observable<any> {
    return this.http.post(this.api+`/login/`, data)
  }
}
