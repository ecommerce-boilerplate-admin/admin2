import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ReportService {
  public api = environment.api + 'report';
  constructor(private http: HttpClient) { }

  getSalesReport(start,end): Observable<any> {
    return this.http.get(this.api + `/dateReport?startDate[date]=${start}&endDate[date]=${end}`);
  }

  getlowstock(): Observable<any> {
    return this.http.get(this.api + `/lowstock`);
  }
  getoutofstock(): Observable<any> {
    return this.http.get(this.api + `/outofstock`);
  }

}
