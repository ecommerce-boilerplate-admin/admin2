import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {
  public api = environment.api;
  constructor(private http: HttpClient) { }

  getallcategory(data): Observable<any> {
    return this.http.get(this.api + `category/all`);
  }
  addcategory(data): Observable<any> {
    return this.http.post(this.api + `category/create`, data);
  }
  updatecategoryByid(id, data): Observable<any> {
    return this.http.put(this.api + `category/update/${id}`, data);
  }
  deletecategoryByid(id): Observable<any> {
    return this.http.delete(this.api + `category/${id}`);
  }
}
