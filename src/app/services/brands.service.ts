import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class BrandsService {
  public api = environment.api
  constructor(private http: HttpClient) {}

  getallbrand():Observable<any> {
    return this.http.get(this.api+`manufactures/all`)
  }
  addbrand(data):Observable<any> {
    return this.http.post(this.api+`manufactures/create/`, data)
  }
  updatebrandByid(id, data):Observable<any> {
    return this.http.put(this.api+`manufactures/update/${id}`, data)
  }
  deletebrandByid(id):Observable<any> {
    return this.http.delete(this.api+`manufactures/${id}`)
  }
}
