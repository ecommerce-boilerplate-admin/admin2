import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BannerService {
  public api = environment.api + 'banner';
  constructor(private http: HttpClient) { }

  getall(): Observable<any> {
    return this.http.get(this.api + `/all`);
  }
  add(data): Observable<any> {
    return this.http.post(this.api + `/create/`, data);
  }
  update(id, data): Observable<any> {
    return this.http.put(this.api + `/update/${id}`, data);
  }
  delete(id): Observable<any> {
    return this.http.delete(this.api + `/${id}`);
  }
}
