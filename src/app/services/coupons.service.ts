import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CouponsService {
  public api = environment.api+'coupon'
  constructor(private http: HttpClient) { }

  getall():Observable<any> {
    return this.http.get(this.api+`/all`)
  }
  add(data):Observable<any> {
    return this.http.post(this.api+`/create/`, data)
  }
  update(id, data):Observable<any> {
    return this.http.put(this.api+`/update/${id}`, data)
  }
  delete(id):Observable<any> {
    return this.http.delete(this.api+`/${id}`)
  }
  bycategory(category):Observable<any> {
    return this.http.get(this.api+`/all?filter[parent_category]=${category}`)
  }
}
