import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  public api = environment.api+'users'
  constructor(private http: HttpClient) { }

  getall():Observable<any> {
    return this.http.get(this.api+`/`)
  }
  getallpaginate(limit,page,sort):Observable<any> {
    return this.http.get(this.api+`/?page=${page}&limit=${limit}&sort[${sort.field}]=${sort.value}`)
  }
  add(data):Observable<any> {
    return this.http.post(this.api+`/create/`, data)
  }
  update(id, data):Observable<any> {
    return this.http.put(this.api+`/update/${id}`, data)
  }
  delete(id):Observable<any> {
    return this.http.delete(this.api+`/${id}`)
  }
  bycategory(category):Observable<any> {
    return this.http.get(this.api+`/all?filter[parent_category]=${category}`)
  }
}
