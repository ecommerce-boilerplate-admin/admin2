import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-viewstore',
  templateUrl: './viewstore.component.html',
  styleUrls: ['./viewstore.component.scss']
})
export class ViewstoreComponent implements OnInit {

  public store_id = null
  constructor(private route: ActivatedRoute) { 
    route.queryParams.subscribe(res => {
      res['id'] = this.store_id
      console.log(this.store_id)
    })
  }

  ngOnInit(): void {
  }

}
