import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { StoreService } from 'src/app/services/store.service';
import * as uikit from 'uikit'

@Component({
  selector: 'app-storelist',
  templateUrl: './storelist.component.html',
  styleUrls: ['./storelist.component.scss']
})
export class StorelistComponent implements OnInit {
  public List = [];
  public id = null
  constructor(private service: StoreService,
    private toast: ToastrService,
    private router: Router,
    private spinner: NgxSpinnerService) { }

  ngOnInit(): void {
    this.getall()
  }

  getall() {
    this.spinner.show();
    this.service.getall().subscribe(res => {
      this.spinner.hide();
      this.List = res.data;
    }, err => {
      this.spinner.hide()
      this.toast.error('Failed to connect with server')
    });
  }

  statusctl(id, status) {
    let formdata = new FormData()
    formdata.append('data', JSON.stringify({status: !status}))
    this.service.update(id, formdata).subscribe(res => {
      if (res.status === 'success') {
        this.getall();
        this.toast.success('Store updated');
      } else {
        this.toast.error('Failed to connect with server')
      }
    });
  }

  edit(item) {
    this.id = item._id;
  }

  delete(id) {
    uikit.modal('#modal-delete').show();
    this.id = id;
  }

  confirm() {
    this.spinner.show();
    this.service.delete(this.id).subscribe(res => {
      this.spinner.hide();
      if (res.status === 'success') {
        this.getall();
        this.toast.success('Store deleted');
        uikit.modal('#modal-delete').hide();
      } else {
        this.toast.error(res.data.message);
        uikit.modal('#modal-delete').hide();
      }
    }, err => {
      this.spinner.hide()
      this.toast.error('Failed to connect with server')
    });
  }

  cancel() {
    uikit.modal('#modal-delete').hide();
  }

  

  async view(id) {
    this.router.navigate(['/dashbaord/viewstore/1'])
  }

 


}
