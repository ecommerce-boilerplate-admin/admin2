import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { StoreService } from 'src/app/services/store.service';

@Component({
  selector: 'app-createstore',
  templateUrl: './createstore.component.html',
  styleUrls: ['./createstore.component.scss']
})
export class CreatestoreComponent implements OnInit {
  public image = null;
  store = new FormGroup({
    store_name: new FormControl('', Validators.required),
    username: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required),
    email: new FormControl('', Validators.required),
    phone: new FormControl('', Validators.required),
    address: new FormControl('', Validators.required),
    location: new FormControl('', Validators.required),
    gstin: new FormControl('', Validators.required),
    pan: new FormControl('', Validators.required),
    district: new FormControl('', Validators.required),
    pin: new FormControl('', Validators.required),
    status: new FormControl(false, Validators.required),
  })
  constructor(private storeService: StoreService,
    private toast: ToastrService,
    private spinner: NgxSpinnerService) { }

  ngOnInit(): void {
  }

  async save() {
    if (this.store.valid && this.image !== null) {
      this.spinner.show();
      const formdata = new FormData();
      formdata.append('image', this.image);
      formdata.append('data', JSON.stringify(this.store.value));
      this.storeService.add(formdata).subscribe(res => {
        this.spinner.hide();
        this.image = null;
        this.store.reset({ name: '', status: true, addedDate: Date.now() });
        if (res.status === 'success') {
          this.spinner.hide();
          this.toast.success('New Store added');
          this.clear()
        } else {
          this.spinner.hide();
          this.toast.error(res.data);
        }
      }, err => {
        this.spinner.hide()
        this.toast.error('Failed to connect with server')
      });
    } else {
      this.toast.warning('some fields are missing');
    }
  }

  async clear() {
    this.store.reset({status: false})
  }


  upload(event) {
    this.image = event.target.files[0];
  }
  

}
