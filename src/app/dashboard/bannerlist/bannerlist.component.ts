import { Component, OnInit } from '@angular/core';
import { BannerService } from 'src/app/services/banner.service';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import * as uikit from 'uikit'

@Component({
  selector: 'app-bannerlist',
  templateUrl: './bannerlist.component.html',
  styleUrls: ['./bannerlist.component.scss']
})
export class BannerlistComponent implements OnInit {
  public list = []
  public id

  constructor(private bannerservice: BannerService,
              private toast: ToastrService,
              private spinner: NgxSpinnerService) { }

  ngOnInit(): void {
    this.getall()
  }

  getall() {
    this.spinner.show()
    this.bannerservice.getall().subscribe(res => {
      this.spinner.hide()
      this.list = res.data
      if(res.data.length === 0) {
        this.toast.warning('No banner in list')
      }
    })
  }

  delete(id) {
    this.id = id
    uikit.modal('#delete').show()
  }

  confirm() {
    this.spinner.show()
    this.bannerservice.delete(this.id).subscribe(res => {
      this.spinner.hide()
      if (res.status === 'success') {
        uikit.modal('#delete').hide()
        this.getall()
        this.toast.success('Banner deleted')
      } else {
        this.toast.error(res.data.message)
      }
    })
  }

  cancel() {
    uikit.modal('#delete').hide()
  }

}
