import { Component, OnInit } from '@angular/core';
import { BannerService } from 'src/app/services/banner.service';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.scss']
})
export class BannerComponent implements OnInit {
  public list = []
  public name = ''
  public image = null
  public imageString  = null

  constructor(private bannerservice: BannerService,
              private toast: ToastrService,
              private spinner: NgxSpinnerService) { }

  ngOnInit(): void {
  }

  upload(event) {
    this.image = event.target.files[0]
    const reader = new FileReader()
    reader.onload = () => this.imageString = reader.result
    reader.readAsDataURL(event.target.files[0])
  }

  save() {
    if(this.name !== '' && this.image !== null) {
      this.spinner.show()
      let formdata = new FormData()
      formdata.append('name', this.name)
      formdata.append('image', this.image)
      this.bannerservice.add(formdata).subscribe(res => {
        console.log(res)
        this.spinner.hide()
        if (res.status === 'success') {
          this.toast.success('banner added to list')
          this.image = null
          this.imageString = null
          this.name = ''
        } else {
          this.toast.error(res.data.message)
        }
      }, err => this.spinner.hide())
    } else {
      this.toast.warning('some fields are missing')
    }
  }

  cancel() {
    this.image = null
    this.imageString = null
    this.name = ''
  }

}
