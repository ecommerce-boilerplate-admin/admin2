import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout/layout.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { CategoryComponent } from './products/category/category.component';
import { SubcategoryComponent } from './products/subcategory/subcategory.component';
import { ProductlistComponent } from './products/productlist/productlist.component';
import { AddproductComponent } from './products/addproduct/addproduct.component';
import { OrdersComponent } from './sales/orders/orders.component';
import { BannerComponent } from './banner/banner.component';
import { CustomersComponent } from './customers/customers.component';
import { ProductreportComponent } from './report/productreport/productreport.component';
import { SalesreportComponent } from './report/salesreport/salesreport.component';
import { InvoiceComponent } from './invoice/invoice.component';
import { UserprofileComponent } from './userprofile/userprofile.component';
import { BrandComponent } from './products/brand/brand.component';
import { BannerlistComponent } from './bannerlist/bannerlist.component';
import { SitesettingsComponent } from './sitesettings/sitesettings.component';
import { CouponsComponent } from './coupons/coupons.component';
import { AdminusersComponent } from './adminusers/adminusers.component';
import { PushnotificationComponent } from './pushnotification/pushnotification.component';
import { CreatestoreComponent } from './storemanagement/createstore/createstore.component';
import { StorelistComponent } from './storemanagement/storelist/storelist.component';
import { CreatepartnerComponent } from './partnermanagement/createpartner/createpartner.component';
import { ListpartnerComponent } from './partnermanagement/listpartner/listpartner.component';
import { ViewstoreComponent } from './storemanagement/viewstore/viewstore.component';


const routes: Routes = [
  {path: '', component: LayoutComponent, children: [
    {path: '', component: DashboardComponent},
    {path: 'products/category', component: CategoryComponent},
    {path: 'products/subcategory', component: SubcategoryComponent},
    {path: 'products/productlist', component: ProductlistComponent},
    {path: 'products/addproduct', component: AddproductComponent},
    {path: 'products/brand', component: BrandComponent},
    {path: 'sales/orders', component: OrdersComponent},
    {path: 'banner/createbanner', component: BannerComponent},
    {path: 'banner/bannerlist', component: BannerlistComponent},
    {path: 'customers', component: CustomersComponent},
    {path: 'report/productreport', component: ProductreportComponent},
    {path: 'report/salesreport', component: SalesreportComponent},
    {path: 'invoice', component: InvoiceComponent},
    {path: 'userprofile', component: UserprofileComponent},
    {path: 'sitesettings', component: SitesettingsComponent},
    {path: 'coupons', component: CouponsComponent},
    {path: 'createstore', component: CreatestoreComponent},
    {path: 'liststore', component: StorelistComponent},
    {path: 'viewstore/:id', component: ViewstoreComponent},
    {path: 'createpartner', component: CreatepartnerComponent},
    {path: 'listpartner', component: ListpartnerComponent},
    {path: 'adminusers', component: AdminusersComponent},
    {path: 'pushnotification', component: PushnotificationComponent}
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
