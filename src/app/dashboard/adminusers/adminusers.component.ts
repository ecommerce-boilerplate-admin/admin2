import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { AdminService } from 'src/app/services/admin.service';
import * as uikit from 'uikit'

@Component({
  selector: 'app-adminusers',
  templateUrl: './adminusers.component.html',
  styleUrls: ['./adminusers.component.scss']
})
export class AdminusersComponent implements OnInit {
  public list = []
  public id
  public flag = true
  public adminUser = new FormGroup({
    username: new FormControl('',Validators.required),
    password: new FormControl('',Validators.required),
    email: new FormControl('',Validators.required),
    phno: new FormControl('',Validators.required)
  })

  constructor(private service: AdminService,
              private spinner: NgxSpinnerService,
              private toast: ToastrService) { }

  ngOnInit(): void {
    this.getall()
  }

  getall() {
    this.spinner.show()
    this.service.getall().subscribe(res => {
      this.spinner.hide()
      console.log(res)
      if(res.status === 'success' && res.data.length > 0) {
        this.list = res.data
      } else if(res.status === 'success' && res.data.length === 0) {
        this.list = res.data
        this.toast.warning('No Admin users')
      } else {
        this.toast.error(res.message)
      }
    }, err => {
      this.spinner.hide()
      this.toast.error('Faild to connect with server', 'Error')
    })
  }

  async save() {
    if(this.adminUser.valid) {
      this.spinner.show()
      this.service.add({admin: this.adminUser.value}).subscribe(res => {
        console.log(res)
        this.spinner.hide()
        if (res.status === 'success') {
          this.toast.success('Admin user added')
          this.getall()
          this.clear()
        } else {
          this.toast.error(res.message)
        }
      })
    } else {
      this.toast.warning('some fields are empty')
    }
  }

  async update() {
    if(this.adminUser.valid) {
      this.spinner.show()
      this.service.update(this.id,{admin: this.adminUser.value}).subscribe(res => {
        console.log(res)
        this.spinner.hide()
        if (res.status === 'success') {
          this.toast.success('Admin user updated')
          this.getall()
          this.clear()
        } else {
          this.toast.error(res.message)
        }
      })
    } else {
      this.toast.warning('some fields are empty')
    }
  }

  async cancel() {
    uikit.modal('#delete').hide()
    this.id = null
  }

  async edit(item) {
    this.id = item._id
    this.flag = false
    this.adminUser.patchValue(item)
    uikit.modal('#create').show()
  }

  async delete(id) {
    this.id = id
    uikit.modal('#delete').show()
  }

  async confirm() {
    this.service.delete(this.id).subscribe(res => {
      if (res.status === 'success') {
        this.getall()
        this.cancel()
        this.toast.success('Admin removed')
      } else {
        this.toast.error(res.message)
      }
    })
  }

  async clear() {
    this.flag = true
    this.adminUser.reset()
    uikit.modal('#create').hide()
  }

}
