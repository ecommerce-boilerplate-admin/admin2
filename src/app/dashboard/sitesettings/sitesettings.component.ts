import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { SitesettingsService } from 'src/app/services/sitesettings.service';

@Component({
  selector: 'app-sitesettings',
  templateUrl: './sitesettings.component.html',
  styleUrls: ['./sitesettings.component.scss']
})
export class SitesettingsComponent implements OnInit {
  @ViewChildren('form') form: QueryList<any>
  public sitesettings = new FormGroup({
   sitename: new FormControl('', Validators.required),
   sitelogo: new FormControl('', Validators.required),
   contactnumber: new FormControl('', Validators.required),
   contactemail: new FormControl('', Validators.required),
   facebooklink: new FormControl('', Validators.required),
   instagramlink: new FormControl('', Validators.required),
   deliverycontrols: new FormControl('')
  })
  public deliverysettings = new FormGroup({
    fromprice: new FormControl('', Validators.required),
    toprice: new FormControl('', Validators.required),
    amount: new FormControl('', Validators.required),
  })
  public siteInitalSave = false
  public deliveycostList = []
  public saveFlag = true
  public deliverySaveFlag = true
  public index
  constructor(private toast: ToastrService,
              private spinner: NgxSpinnerService,
              private service: SitesettingsService) { }

  ngOnInit(): void {
    this.getsite()
  }
  ngAfterViewInit(): void {
    this.form.forEach(el => el.nativeElement.disabled = true)
  }

  getsite() {
    this.service.getall().subscribe(res => {
      console.log(res)
      if(res.status === 'success' && res.data.length > 0) {
        this.siteInitalSave = true
        this.sitesettings.patchValue(res.data[0])
        this.deliveycostList = JSON.parse(res.data[0].deliverycontrols)
      } else if(res.status === 'success' && res.data.length === 0 ) {
        this.siteInitalSave = false
        this.edit()
      } else {
        this.toast.error(res.message)
      }
    })
  }

  async save() {
    if(this.sitesettings.valid) {
      this.spinner.show()
      this.sitesettings.patchValue({deliverycontrols: JSON.stringify(this.deliveycostList)})
      this.service.add({site: this.sitesettings.value}).subscribe(res => {
        this.spinner.hide()
        console.log(res)
        if(res.status === 'success') {
          this.getsite()
        } else {
          this.toast.error(res.message)
        }
      })
    } else {
      this.toast.warning('some fields are missing')
    }
  }

  async edit() {
    this.saveFlag = false
    this.form.forEach(el => el.nativeElement.disabled = false)
  }

  async cancel() {
    this.saveFlag = true
    this.form.forEach(el => el.nativeElement.disabled = true)
  }

  async update() {
    this.sitesettings.patchValue({deliverycontrols: JSON.stringify(this.deliveycostList)})
    this.service.add({site: this.sitesettings}).subscribe()
  }

  async uploadImage(event) {
    const reader = new FileReader()
    reader.onload = () => this.sitesettings.patchValue({sitelogo: reader.result})
    reader.readAsDataURL(event.target.files[0])
  }

  async addDelivery() {
    if(this.deliverysettings.valid) {
      this.deliveycostList.push(this.deliverysettings.value)
      this.deliverysettings.reset()
    } else {
      this.toast.warning('some fields are missing')
    }
  }

  async cancelDelivery() {
    this.deliverySaveFlag = true
    this.deliverysettings.reset()
  }

  async updateDelivery() {
    this.deliveycostList[this.index] = this.deliverysettings.value
    this.cancelDelivery()

  }

  async editItem(index,data) {
    this.index = index
    this.deliverySaveFlag = false
    this.deliverysettings.patchValue(data)
  }
  async deleteItem(index) {
    this.deliveycostList.splice(index, 1)
  }

}
