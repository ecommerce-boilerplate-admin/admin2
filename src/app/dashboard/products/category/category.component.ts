import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { CategoryService } from 'src/app/services/category.service';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import * as uikit from 'uikit';
import { __core_private_testing_placeholder__ } from '@angular/core/testing';


@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit {
  public saveFlag = true;
  public categoryList = [];
  public category = new FormGroup({
    name: new FormControl('', Validators.required),
    status: new FormControl(true),
    addedDate: new FormControl(Date.now())
  });
  public icon = null;
  public categoryId;

  constructor(
    private categoryservice: CategoryService,
    private toast: ToastrService,
    private spinner: NgxSpinnerService) { }

  ngOnInit(): void {
    this.getallcategory();
  }

  getallcategory() {
    this.spinner.show();
    this.categoryservice.getallcategory({}).subscribe(res => {
      this.spinner.hide();
      this.categoryList = res.data;
    }, err => {
      this.spinner.hide()
      this.toast.error('Failed to connect with server')
    });
  }

  statusctl(id, status) {
    this.categoryservice.updatecategoryByid(id, { status: !status }).subscribe(res => {
      if (res.status === 'success') {
        this.getallcategory();
        this.toast.success('category updated');
      } else {
        this.toast.error('Failed to connect with server')
      }
    });
  }

  edit(item) {
    this.categoryId = item._id;
    this.category.patchValue(item);
    this.saveFlag = false;
    uikit.modal('#modal-center').show();
  }

  delete(id) {
    uikit.modal('#modal-delete').show();
    this.categoryId = id;
  }

  confirm() {
    this.spinner.show();
    this.categoryservice.deletecategoryByid(this.categoryId).subscribe(res => {
      this.spinner.hide();
      if (res.status === 'success') {
        this.getallcategory();
        this.toast.success('category deleted');
        uikit.modal('#modal-delete').hide();
      } else {
        this.toast.error(res.data.message);
        uikit.modal('#modal-delete').hide();
      }
    });
  }

  cancel() {
    uikit.modal('#modal-delete').hide();
  }

  clear() {
    this.category.reset({ name: '', status: true, addedDate: Date.now() });
    this.icon = null
  }

  upload(event) {
    this.icon = event.target.files[0];
  }

  save() {
    if (this.category.valid && this.icon !== null) {
      this.spinner.show();
      const formdata = new FormData();
      formdata.append('icon', this.icon);
      formdata.append('name', this.category.value.name);
      formdata.append('addedDate', this.category.value.addedDate);
      formdata.append('status', this.category.value.status);
      this.categoryservice.addcategory(formdata).subscribe(res => {
        this.spinner.hide();
        this.icon = null;
        this.category.reset({ name: '', status: true, addedDate: Date.now() });
        if (res.status === 'success') {
          this.spinner.hide();
          this.getallcategory();
          this.toast.success('New category added');
          uikit.modal('#modal-center').hide();
        } else {
          this.spinner.hide();
          this.toast.error(res.data.message);
        }
      }, err => {
        this.spinner.hide()
        this.toast.error('Failed to connect with server')
      });
    } else {
      this.toast.warning('some fields are missing');
    }
  }

  update() {
    if (this.category.valid) {
      this.spinner.show();
      this.saveFlag = true;
      const formdata = new FormData();
      if (this.icon !== null) {
        formdata.append('icon', this.icon);
      }
      formdata.append('name', this.category.value.name);
      formdata.append('addedDate', this.category.value.addedDate);
      formdata.append('status', this.category.value.status);
      this.categoryservice.updatecategoryByid(this.categoryId, formdata).subscribe(res => {
        this.spinner.hide();
        this.icon = null;
        this.category.reset({ name: '', status: true, addedDate: Date.now() });
        if (res.status === 'success') {
          console.log(res)
          this.getallcategory();
          this.toast.success('category updated');
          uikit.modal('#modal-center').hide();
        } else {
          this.toast.error(res.data.message);
        }
      }, err => {
        this.spinner.hide()
        this.toast.error('Failed to connect with server')
      });
    } else {
      this.toast.warning('some fields are missing');
    }
  }

  async openCreate() {
    uikit.modal('#modal-center',{"bg-close": false}).show()
  }

  async closeCreate() {
    uikit.modal('#modal-center').hide()
    this.saveFlag = true
    this.clear()
  }

}
