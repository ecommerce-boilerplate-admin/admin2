import { Component, OnInit } from '@angular/core';
import { BrandsService } from 'src/app/services/brands.service';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import * as uikit from 'uikit'
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-brand',
  templateUrl: './brand.component.html',
  styleUrls: ['./brand.component.scss']
})
export class BrandComponent implements OnInit {
  public saveFlag = true
  public brandList = []
  public brand = new FormGroup({
    name: new FormControl('', Validators.required),
    status: new FormControl(true),
    date: new FormControl(Date.now())
  })
  public image = null
  public brandId

  constructor(private brandservice: BrandsService,
              private toast: ToastrService,
              private spinner: NgxSpinnerService) { }

  ngOnInit(): void {
    this.getallbrands()
  }

  getallbrands() {
    this.brandservice.getallbrand().subscribe(res => {this.brandList = res.data}, err => {
      this.spinner.hide()
      this.toast.error('Failed to connect with server')
    })
  }

  statusctl(id,status) {
   this.brandservice.updatebrandByid(id, {status: status}).subscribe(res => {
     if(res.status === 'success') {
       this.getallbrands()
       this.toast.success('Manufature updated')
     }
   }, err => {
    this.spinner.hide()
    this.toast.error('Failed to connect with server')
  })
  }

  edit(item) {
    this.brand.patchValue(item)
    this.brandId = item._id
    uikit.modal('#create-brand').show()
    this.saveFlag = false
  }

  delete(id) {
   uikit.modal('#brand-delete').show()
   this.brandId = id 
  }

  confirm() {
    this.brandservice.deletebrandByid(this.brandId).subscribe(res => {
      if (res.status === 'success') {
        this.getallbrands()
        this.toast.success('category deleted')
        uikit.modal('#brand-delete').hide()
      } else {
        this.toast.error(res.data.message)
        uikit.modal('#brand-delete').hide()
      }
    }, err => {
      this.spinner.hide()
      this.toast.error('Failed to connect with server')
    })
  }

  cancel() {
    uikit.modal('#brand-delete').hide()
  }

  clear() {
    this.brand.reset({name: '', date: Date.now(), status: true})
  }

  upload(event) {
    this.image = event.target.files[0]
  }

  save() {
    console.log(this.brand.value)
    if(this.brand.valid && this.image !== null) {
      this.spinner.show()
      let formdata = new FormData()
      formdata.append('image', this.image)
      formdata.append('name', this.brand.value.name)
      formdata.append('date', this.brand.value.date)
      formdata.append('status', this.brand.value.status)
      this.brandservice.addbrand(formdata).subscribe(res => {
        console.log(res)
        this.brand.reset({name: '', date: Date.now(), status: true})
        if(res.status === "success") {
          this.spinner.hide()
          this.getallbrands()
          this.toast.success('New Manufature added')
          uikit.modal('#create-brand').hide()
        } else {
          this.spinner.hide()
          this.toast.error(res.data.message)
        }
      }, err => {
        this.spinner.hide()
        this.toast.error('Failed to connect with server')
      })
    } else {
      this.toast.warning('some fields are missing')
    }
  }

  update() {
    if(this.brand.valid) {
      this.spinner.show()
      this.saveFlag = true
      let formdata = new FormData()
      if(this.image !== null) {
        formdata.append('image', this.image)
      }
      formdata.append('name', this.brand.value.name)
      formdata.append('date', this.brand.value.date)
      formdata.append('status', this.brand.value.status)
      this.brandservice.updatebrandByid(this.brandId,formdata).subscribe(res => {
        this.spinner.hide()
        this.image = null
        this.brand.reset({name: '', date: Date.now(), status: true})
        if(res.status === "success") {
          this.getallbrands()
          this.toast.success('manufature updated')
          uikit.modal('#create-brand').hide()
        } else {
          this.toast.error(res.data.message)
        }
      }, err => {
        this.spinner.hide()
        this.toast.error('Failed to connect with server')
      })
    } else {
      this.toast.warning('some fields are missing')
    }
  }

}
