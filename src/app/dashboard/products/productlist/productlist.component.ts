import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ProductService } from 'src/app/services/product.service';
import { CategoryService } from 'src/app/services/category.service';
import { SubcategoryService } from 'src/app/services/subcategory.service';
import { BrandsService } from 'src/app/services/brands.service';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import * as uikit from 'uikit'

@Component({
  selector: 'app-productlist',
  templateUrl: './productlist.component.html',
  styleUrls: ['./productlist.component.scss']
})
export class ProductlistComponent implements OnInit {
 public list = []
 public id
 public flag = true
 public categoryList = []
 public subcategoryList = []
 public brandList = []
 public featuredImage = null
 public gallery = []
 public product = new FormGroup({
   name: new FormControl('', Validators.required),
   sku: new FormControl('', Validators.required),
   category: new FormControl(null, Validators.required),
   subcategory: new FormControl(null, Validators.required),
   manufacture: new FormControl(null, Validators.required),
   price: new FormControl(0, Validators.required),
   discountPrice: new FormControl(0),
   unit: new FormControl('', Validators.required),
   stock: new FormControl(0, Validators.required),
   description: new FormControl(''),
   tax: new FormControl(0),
   storeid: new FormControl(0)
 })
  constructor(private productservice: ProductService,
              private catservice: CategoryService,
              private subservice: SubcategoryService,
              private brandservice: BrandsService,
              private toast: ToastrService,
              private spinner: NgxSpinnerService) { }

  ngOnInit(): void {
    this.getall()
  }

  getall() {
    this.spinner.show()
    this.catservice.getallcategory({}).subscribe(res => this.categoryList = res.data)
    this.brandservice.getallbrand().subscribe(res => this.brandList = res.data)
    this.productservice.getall().subscribe(res => {
      this.list = res.data
      this.spinner.hide()
    }, err => {
      this.spinner.hide()
      this.toast.error('Failed to connect with server')
    })
  }

  async getallByCategory(value) {
    if(value !== 'all') {
      this.spinner.show()
      this.productservice.getallCategory(value).subscribe(res => {
        this.spinner.hide()
        if(res.status === 'success' && res.data.length > 0) {
          this.toast.success('Some data fetched')
          this.list = res.data
        } else if(res.status === 'success' && res.data.length === 0) {
          this.toast.warning('No Product in this category')
          this.list = res.data
        }
      }, err => {
        this.spinner.hide()
        this.toast.error('Failed to connect with server')
        this.getall()
      })
    } else {
      this.getall()
    }
  }

  async openCreate() {
    uikit.modal('#create',{"bg-close" : false}).show()
  }

  async closeCreate() {
    uikit.modal('#create').hide()
    this.clear()
    this.flag = true
  }

  uploadImage(event) {
    this.featuredImage = event.target.files[0]
  }

  uploadGallery(event) {
    let i = 0
    for(let j of event.target.files) {
      this.gallery.push(event.target.files[i])
      i++
    }
  }

  subcatImporter() {
    this.subservice.bycategory(this.product.value.category).subscribe(res => {
      this.subcategoryList = res.data
    }, err => {
      this.spinner.hide()
      this.toast.error('Failed to connect with server')
    })
  }

  save() {
    if(this.product.valid) {
      this.spinner.show()
      let formdata = new FormData()
      formdata.append('featuredImage', this.featuredImage)
      if(this.gallery.length > 0) {
        this.gallery.map(item => formdata.append('gallery', item))
      }
      formdata.append('data',JSON.stringify(this.product.value))
      this.productservice.add(formdata).subscribe(res => {
        this.spinner.hide()
        if(res.status === 'success') {
          this.clear()
          this.getall()
          this.toast.success('Product added')
          uikit.modal('#create').hide()
        } else {
          this.toast.error(res.data)
        }
      }, err => {
        this.spinner.hide()
        this.toast.error('Failed to connect with server')
      })
    } else {
      this.toast.warning('some fields are missing')
    }
  }

  update() {
    if(this.product.valid) {
      this.spinner.show()
      let formdata = new FormData()
      formdata.append('featuredImage', this.featuredImage)
      if(this.gallery.length > 0) {
        this.gallery.map(item => formdata.append('gallery', item))
      }
      formdata.append('data',JSON.stringify(this.product.value))
      this.productservice.update(this.id,formdata).subscribe(res => {
        this.spinner.hide()
        if(res.status === 'success') {
          this.clear()
          this.getall()
          this.toast.success('Product added')
          uikit.modal('#create').hide()
        }
      }, err => {
        this.spinner.hide()
        this.toast.error('Failed to connect with server')
      })
    } else {
      this.toast.warning('some fields are missing')
    }
  }

  confirm() {
    this.spinner.show()
    this.productservice.delete(this.id).subscribe(res => {
      this.spinner.hide()
      if (res.status === 'success') {
        this.toast.success('product deleted')
        this.getall()
        uikit.modal('#delete').hide()
      } else {
        this.toast.error(res.data.message)
      }
    }, err => {
      this.spinner.hide()
      this.toast.error('Failed to connect with server')
    })
  }

  cancel() {
    uikit.modal('#delete').hide()
  }

  clear() {
    this.product.reset({
      name: '',
      category: null,
      subcategory: null,
      brand: null,
      price: 0,
      discountPrice: 0,
      stock: 0,
      unit: '',
      description: '',
      status: true,
      date: ""+Date.now()
    })
  }

  edit(item) {
    this.id = item._id
    this.flag = false
    this.product.patchValue(item)
    uikit.modal('#create').show()
  }

  delete(item) {
    this.id = item._id
    uikit.modal('#delete').show()
  }

}
