import { Component, OnInit } from '@angular/core';
import { BrandsService } from 'src/app/services/brands.service';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { SubcategoryService } from 'src/app/services/subcategory.service';
import { CategoryService } from 'src/app/services/category.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import * as uikit from 'uikit';

@Component({
  selector: 'app-subcategory',
  templateUrl: './subcategory.component.html',
  styleUrls: ['./subcategory.component.scss']
})
export class SubcategoryComponent implements OnInit {
  public list = [];
  public categoryList = [];
  public id;
  public flag = true;
  public subcategory = new FormGroup({
    subcategory_name: new FormControl('', Validators.required),
    parent_category: new FormControl(null, [Validators.required, Validators.nullValidator]),
    status: new FormControl(true),
    added_date: new FormControl(new Date())
  });

  constructor(
    private service: SubcategoryService,
    private catservice: CategoryService,
    private toast: ToastrService,
    private spinner: NgxSpinnerService) { }

  ngOnInit(): void {
    this.getAll();
  }

  getAll() {
    this.spinner.show();
    this.service.getall().subscribe(res => {
      this.spinner.hide();
      this.list = res.data;
    }, err => {
      this.spinner.hide()
      this.toast.error('Failed to connect with server')
    });
    this.catservice.getallcategory({}).subscribe(res => {this.categoryList = res.data}, err => {
      this.spinner.hide()
      this.toast.error('Failed to connect with server')
    });
  }

  async openCreate() {
    uikit.modal('#subcat-create').show()
  }

  async closeCreate() {
    this.clear()
    uikit.modal('#subcat-create').hide()
    this.flag = true
  }

  statusctl(id, state) {
    this.service.update(id, { status: !state }).subscribe(res => {
      console.log(res);
      if (res.status === 'success') {
        this.toast.success('subcategory updated');
        this.getAll();
      } else {
        this.toast.error(res.data.message);
      }
    }, err => {
      this.spinner.hide()
      this.toast.error('Failed to connect with server')
    });
  }

  edit(item) {
    this.id = item._id;
    this.flag = false;
    this.subcategory.patchValue(item);
    uikit.modal('#subcat-create').show();
  }

  delete(item) {
    this.id = item._id;
    uikit.modal('#delete').show();
  }

  confirm() {
    this.spinner.show();
    this.service.delete(this.id).subscribe(res => {
      this.spinner.hide();
      this.cancel();
      if (res.status === 'success') {
        this.getAll();
        this.toast.success('Deleted successfully');
      } else {
        this.toast.error(res.data.message);
      }
    }, err => {
      this.spinner.hide()
      this.toast.error('Failed to connect with server')
    });
  }

  cancel() {
    uikit.modal('#delete').hide();
  }

  async clear() {
    this.subcategory.reset({subcategory_name: '', parent_category: null, status: true, added_Date: await Date.now()})
    this.subcategory.patchValue({added_date: Date.now()})
  }

  save() {
    if(this.subcategory.valid) {
      this.spinner.show()
      this.service.add(this.subcategory.value).subscribe(res => {
        this.spinner.hide()
        this.clear()
        if(res.status === 'success') {
          this.getAll()
          uikit.modal('#subcat-create').hide()
          this.toast.success('subcategory added')
        } else {
          console.log(res)
          this.toast.error(res.data)
        }
      }, err => {
        this.spinner.hide()
        this.toast.error('Failed to connect with server')
      });
    } else {
      this.toast.warning('some fields are missing');
    }
  }

  update() {
    if (this.subcategory.valid) {
      this.spinner.show();
      this.service.update(this.id, this.subcategory.value).subscribe(res => {
        console.log(res);
        this.spinner.hide();
        this.clear();
        this.flag = true;
        if (res.status === 'success') {
          this.getAll();
          uikit.modal('#subcat-create').hide();
          this.toast.success('subcategory updated');
        } else {
          this.toast.error(res.message)
        }
      }, err => {
        this.spinner.hide()
        this.toast.error('Failed to connect with server')
      });
    } else {
      this.toast.warning('some fields are missing');
    }
  }

}
