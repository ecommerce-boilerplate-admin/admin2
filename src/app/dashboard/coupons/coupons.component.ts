import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { CouponsService } from 'src/app/services/coupons.service';
import * as uikit from 'uikit'

@Component({
  selector: 'app-coupons',
  templateUrl: './coupons.component.html',
  styleUrls: ['./coupons.component.scss']
})
export class CouponsComponent implements OnInit {

  public list
  public id
  public flag = true
  public item = new FormGroup({
    couponname: new FormControl('', Validators.required),
    couponsecret: new FormControl('', Validators.required),
    coupondiscount: new FormControl('', Validators.required),
    couponcount: new FormControl(''),
    couponused: new FormControl(0),
    coupondate: new FormControl(Date.now(), Validators.required)
  })

  constructor(private service: CouponsService,
              private spinner: NgxSpinnerService,
              private toast: ToastrService) { }

  ngOnInit(): void {
    this.getall()
  }

  getall() {
    this.spinner.show()
    this.service.getall().subscribe(res => {
      this.spinner.hide()
      if(res.status === 'success' && res.data.length > 0) {
        this.list = res.data
      } else if(res.status === 'success' && res.data.length === 0) {
        this.list = res.data
        this.toast.warning('No coupons')
      } else {
        this.toast.error(res.message)
      }
    }, err => {
      this.spinner.hide()
      this.toast.error('FAiled to connect with server')
    })
  }
  async save() {
    if (this.item.valid) {
      this.spinner.show()
      this.service.add({coupon: this.item.value}).subscribe(res => {
        this.spinner.hide()
        uikit.modal('#modal-center').hide()
        if (res.status === 'success') {
          this.toast.success('Coupon added')
          this.clear()
          this.getall()
        } else {
          this.toast.error(res.message)
        }
      },err => {
        this.spinner.hide()
        this.toast.error('FAiled to connect with server')
      })
    } else {
      this.toast.warning('some fields are missing')
    }
  }

  async update() {
    if (this.item.valid) {
      this.spinner.show()
      this.service.update(this.id,{coupon: this.item.value}).subscribe(res => {
        this.spinner.hide()
        uikit.modal('#modal-center').hide()
        if (res.status === 'success') {
          this.toast.success('Coupon updated')
          this.clear()
          this.getall()
        } else {
          this.toast.error(res.message)
        }
      },err => {
        this.spinner.hide()
        this.toast.error('FAiled to connect with server')
      })
    } else {
      this.toast.warning('some fields are missing')
    }
  }

  async clear() {
    this.item.patchValue({
      couponname: '',
      couponsecret: '',
      coupondiscount: 0,
      couponcount: 0,
      coupondate: Date.now()
    })
  }

  async edit(item) {
    this.item.patchValue(item)
    this.id = item._id
    this.flag = false
    uikit.modal('#modal-center').show()
  }

  delete(item) {
    this.id = item._id
    uikit.modal('#delete').show()
  }

  async confirm() {
    this.spinner.show()
    this.service.delete(this.id).subscribe(res => {
      this.spinner.hide()
      if(res.status === 'success') {
        this.getall()
        this.toast.success('Coupon deleted')
        this.cancel()
      } else {
        this.toast.error(res.message)
      }
    },
    err => {
      this.spinner.hide()
      this.toast.error('FAiled to connect with server')
    })
  }

  async cancel() {
    this.id = null
    uikit.modal('#delete').hide()
  }

  async view(value) {
    uikit.modal('#viewusers').show()
  }
}
