import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import * as uikit from 'uikit'

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.scss']
})
export class CustomersComponent implements OnInit {
  list = []
  id 
  paginate = {
    page: 1,
    limit: 10,
    sort: {field: 'fname', value: 1}
  }
  public count = []

  constructor(private service: UserService,
              private spinner: NgxSpinnerService,
              private toast: ToastrService) { }

  ngOnInit(): void {
    this.getall()
  }

  getall() {
    this.spinner.show()
    this.service.getall().subscribe(res => {
      this.spinner.hide()
      if(res.status === 'success' && res.data.data.length > 0) {
        this.genArray(Math.floor(res.data.data.length/10))
      } else if (res.status === 'success' && res.data.data.length === 0) {
        this.toast.warning('Customer list is empty')
      } else {
        this.toast.error(res.message)
      }
    })
  }

  delete(id) {
    this.id = id
    uikit.modal('#delete').show()
  }
   confirm() {
     this.spinner.show()
     this.service.delete(this.id).subscribe(res => {
       this.spinner.hide()
       this.cancel()
       if((res.status === 'success')) {
         this.toast.success('Customer Deleted')
         this.getall()
       } else {
         this.cancel()
         this.toast.error(res.message)
       }
     })
   }

   cancel() {
     uikit.modal('#delete').hide()
   }

   genArray(count) {
    for(let i = 0;i<= count;i++) {
      this.count.push(1)
    }
    this.getListbyQuery()
   }
 

  getListbyQuery() {
    this.service.getallpaginate(this.paginate.limit,this.paginate.page,this.paginate.sort).subscribe(res => {
      if(res.status === 'success' && res.data.data.length > 0) {
        this.list = res.data.data
      } else if (res.status === 'success' && res.data.data.length === 0) {
        this.toast.warning('No more customers')
      } else {
        this.toast.error(res.message)
      }
    })
  }

  async next() {
    this.paginate.page += 1
    this.service.getallpaginate(this.paginate.limit,this.paginate.page,this.paginate.sort).subscribe(res => {
      if(res.status === 'success' && res.data.data.length > 0) {
        this.list = res.data.data
      } else if (res.status === 'success' && res.data.data.length === 0) {
        this.toast.warning('No more customers')
        this.paginate.page = this.count.length
      } else {
        this.toast.error(res.message)
      }
    })
  }

  async previous() {
    this.paginate.page -= 1
    this.service.getallpaginate(this.paginate.limit,this.paginate.page,this.paginate.sort).subscribe(res => {
      if(res.status === 'success' && res.data.data.length > 0) {
        this.list = res.data.data
      } else if (res.status === 'success' && res.data.data.length === 0) {
        this.toast.warning('No more customers')
        this.paginate.page = 1
      } else {
        this.toast.warning('no data')
        this.paginate.page = 1
      }
    })
  }

  async manual(page) {
    this.paginate.page = page
    this.getListbyQuery()

  }



}
