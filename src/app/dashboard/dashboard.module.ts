import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgxSpinnerModule } from 'ngx-spinner'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'

import { DashboardRoutingModule } from './dashboard-routing.module';
import { LayoutComponent } from './layout/layout.component';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { CategoryComponent } from './products/category/category.component';
import { SubcategoryComponent } from './products/subcategory/subcategory.component';
import { ProductlistComponent } from './products/productlist/productlist.component';
import { AddproductComponent } from './products/addproduct/addproduct.component';
import { OrdersComponent } from './sales/orders/orders.component';
import { BannerComponent } from './banner/banner.component';
import { CustomersComponent } from './customers/customers.component';
import { ProductreportComponent } from './report/productreport/productreport.component';
import { SalesreportComponent } from './report/salesreport/salesreport.component';
import { InvoiceComponent } from './invoice/invoice.component';
import { UserprofileComponent } from './userprofile/userprofile.component';
import { BrandComponent } from './products/brand/brand.component';
import { BannerlistComponent } from './bannerlist/bannerlist.component';
import { SitesettingsComponent } from './sitesettings/sitesettings.component';
import { CouponsComponent } from './coupons/coupons.component';
import { AdminusersComponent } from './adminusers/adminusers.component';
import { PushnotificationComponent } from './pushnotification/pushnotification.component';
import { StorelistComponent } from './storemanagement/storelist/storelist.component';
import { CreatestoreComponent } from './storemanagement/createstore/createstore.component';
import { CreatepartnerComponent } from './partnermanagement/createpartner/createpartner.component';
import { ListpartnerComponent } from './partnermanagement/listpartner/listpartner.component';
import { ViewstoreComponent } from './storemanagement/viewstore/viewstore.component';


@NgModule({
  declarations: [LayoutComponent, FooterComponent, HeaderComponent, SidebarComponent, DashboardComponent, CategoryComponent, SubcategoryComponent, ProductlistComponent, AddproductComponent, OrdersComponent, BannerComponent, CustomersComponent, ProductreportComponent, SalesreportComponent, InvoiceComponent, UserprofileComponent, BrandComponent, BannerlistComponent, SitesettingsComponent, CouponsComponent, AdminusersComponent, PushnotificationComponent, StorelistComponent, CreatestoreComponent, CreatepartnerComponent, ListpartnerComponent, ViewstoreComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    DashboardRoutingModule,
    NgxSpinnerModule
  ]
})
export class DashboardModule { }
