import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { ProductService } from 'src/app/services/product.service';
import { ReportService } from 'src/app/services/report.service';

@Component({
  selector: 'app-productreport',
  templateUrl: './productreport.component.html',
  styleUrls: ['./productreport.component.scss']
})
export class ProductreportComponent implements OnInit {

  productReports = [];
  SelectedReport = 'stock'


  constructor(
    private reportService: ReportService,
    private productservice: ProductService,
    private loader: NgxSpinnerService,
    private toast: ToastrService
  ) { }

  ngOnInit(): void {
    this.getallStockReport();
  }

  /**
   * get all product reports from API
   */

  async reportCtl() {
    switch(this.SelectedReport) {
      case 'stock': this.getallStockReport()
      break
      case 'low': this.getallLowstockReport()
      break
      case 'out': this.getallOutofstockReport()
      break
      default: this.getallStockReport()
    }
  }

  async getallStockReport() {
    this.loader.show()
    this.productservice.getall().subscribe(res => {
      this.loader.hide()
      if (res.status === 'success' && res.data.length > 0) {
        this.productReports = res.data
      } else if (res.status === 'success' && res.data.length === 0) {
        this.toast.warning('no data')
      } else {
        this.toast.error(res.message)
      }
    })
  }

  async getallLowstockReport() {
    this.reportService.getlowstock().subscribe(res => {
      this.loader.hide()
      if (res.status === 'success' && res.data.length > 0) {
        this.productReports = res.data
      } else if (res.status === 'success' && res.data.length === 0) {
        this.toast.warning('no data')
      } else {
        console.log(res)
        this.toast.error(res.message)
      }
    })
  }

  async getallOutofstockReport() {
    this.reportService.getoutofstock().subscribe(res => {
      this.loader.hide()
      if (res.status === 'success' && res.data.length > 0) {
        this.productReports = res.data
      } else if (res.status === 'success' && res.data.length === 0) {
        this.toast.warning('no data')
      } else {
        console.log(res)
        this.toast.error(res.message)
      }
    })
  }
}
