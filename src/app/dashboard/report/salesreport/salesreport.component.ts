import { Component, OnInit } from '@angular/core';
import { ReportService } from 'src/app/services/report.service';
import *  as moment from 'moment'
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-salesreport',
  templateUrl: './salesreport.component.html',
  styleUrls: ['./salesreport.component.scss']
})
export class SalesreportComponent implements OnInit {
public initDate = {
  start: moment().format(),
  end: moment().format()
}
public reportList = []
  constructor(
    private reportService: ReportService,
    private spinner: NgxSpinnerService,
    private toast: ToastrService
  ) { }

  ngOnInit(): void {
    this.getAll();
  }

  getAll() {
    this.spinner.show()
    this.reportService.getSalesReport(this.initDate.start, this.initDate.end).subscribe(res => {
      this.spinner.hide()
      if (res.status === 'success' && res.data.length > 0) {
        this.reportList = res.data
        console.log(res)
      } else if (res.status === 'success' && res.data.length === 0) {
        this.toast.warning('No sales')
      } else {
        this.toast.error(res.message)
      }
    });
  }

  async apply(startdate, stopdate) {
    this.initDate.start = moment(startdate).format()
    this.initDate.end = moment(stopdate).format()
    this.getAll()
  }

}
