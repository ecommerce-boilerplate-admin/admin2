import { Component, OnInit } from '@angular/core';
import { ProductService } from 'src/app/services/product.service';
import { ReportService } from 'src/app/services/report.service';
import { UserService } from 'src/app/services/user.service';
import * as moment from 'moment'

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  public card = {
    users: 0,
    products: 0,
    toadysale: 0,
    lowstock: 0
  }

  constructor(private userservice: UserService,
              private productservice: ProductService,
              private reportservice: ReportService) { }

  ngOnInit(): void {
    this.getallproducts()
    this.getallusers()
    this.getalllowstock()
    this.getallsalestoday()
  }

  getallproducts() {
    this.productservice.getall().subscribe(res => this.card.products = res.data.length)
  }
  getallusers() {
    this.userservice.getall().subscribe(res => this.card.users = res.data.data.length)
  }
  getalllowstock() {
    this.reportservice.getlowstock().subscribe(res => this.card.lowstock = res.data.length)
  }

  getallsalestoday() {
    this.reportservice.getSalesReport(moment().format(),moment().format()).subscribe(res => {
      if (res.status === 'success' && res.data.length > 0) {
        res.data.map(item => this.card.toadysale += item.total_due)
      }
    })
  }

}
