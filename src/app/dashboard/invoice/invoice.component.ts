import { Component, OnInit } from '@angular/core';
import { InvoiceService } from 'src/app/services/invoice.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import * as uikit from 'uikit'

@Component({
  selector: 'app-invoice',
  templateUrl: './invoice.component.html',
  styleUrls: ['./invoice.component.scss']
})
export class InvoiceComponent implements OnInit {
  public list = []
  public id
  paginate = {
    page: 1,
    limit: 10,
    sort: {field: 'invoice_id', value: -1}
  }
  public count = []

  constructor(private service: InvoiceService,
    private spinner: NgxSpinnerService,
    private toast: ToastrService) { }

  ngOnInit(): void {
    this.getall()
  }

  getall() {
    this.spinner.show()
    this.service.getall().subscribe(res => {
      this.spinner.hide()
      if(res.status === 'success' && res.data.data.length > 0) {
        this.genArray(Math.floor(res.data.data.length/10))
      } else if (res.status === 'success' && res.data.data.length === 0) {
        this.toast.warning('No invoice at this time')
      } else {
        this.toast.error(res.message)
      }
    })
  }

  async delete(item) {
    console.log('kkkk')
    this.id = item._id
    uikit.modal('#delete').show()
  }

  async confirm() {
    this.spinner.show()
    this.service.delete(this.id).subscribe(res => {
      this.spinner.hide()
      uikit.modal('#delete').hide()
      if(res.status === 'success') {
        this.toast.success('Deleted successfully')
        this.getall()
      } else {
        this.toast.error(res.message)
      }
    })
  }
  async cancel() {
    uikit.modal('#delete').hide()
  }

  genArray(count) {
    for(let i = 0;i<= count;i++) {
      this.count.push(1)
    }
    this.getListbyQuery()
   }
 

  getListbyQuery() {
    this.service.getallpaginate(this.paginate.limit,this.paginate.page,this.paginate.sort).subscribe(res => {
      if(res.status === 'success' && res.data.data.length > 0) {
        this.list = res.data.data
      } else if (res.status === 'success' && res.data.data.length === 0) {
        this.toast.warning('No invoice at this time')
      } else {
        this.toast.error(res.message)
      }
    })
  }

  async next() {
    this.paginate.page += 1
    this.service.getallpaginate(this.paginate.limit,this.paginate.page,this.paginate.sort).subscribe(res => {
      if(res.status === 'success' && res.data.data.length > 0) {
        this.list = res.data.data
      } else if (res.status === 'success' && res.data.data.length === 0) {
        this.toast.warning('No invoice at this time')
        this.paginate.page = this.count.length
      } else {
        this.toast.error(res.message)
      }
    })
  }

  async previous() {
    this.paginate.page -= 1
    this.service.getallpaginate(this.paginate.limit,this.paginate.page,this.paginate.sort).subscribe(res => {
      if(res.status === 'success' && res.data.data.length > 0) {
        this.list = res.data.data
      } else if (res.status === 'success' && res.data.data.length === 0) {
        this.toast.warning('No invoice at this time')
        this.paginate.page = 1
      } else {
        this.toast.warning('no data')
        this.paginate.page = 1
      }
    })
  }

  async manual(page) {
    this.paginate.page = page
    this.getListbyQuery()

  }

}
