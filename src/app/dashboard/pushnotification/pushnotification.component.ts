import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { PushService } from 'src/app/services/push.service';

@Component({
  selector: 'app-pushnotification',
  templateUrl: './pushnotification.component.html',
  styleUrls: ['./pushnotification.component.scss']
})
export class PushnotificationComponent implements OnInit {
  public  notificationPayload = {
    notification: {
        title: "New in lamartindia",
        body: "Offer Available!",
        icon: "assets/main-page-logo-small-hat.png",
        vibrate: [100, 50, 100],
        data: {
            dateOfArrival: Date.now(),
            primaryKey: 1
        },
        actions: [{
            action: "explore",
            title: "Go to the site"
        }]
    }
  }

  constructor(private service: PushService,
              private spinner: NgxSpinnerService,
              private toast: ToastrService) { }

  ngOnInit(): void {
  }

  async send() {
    this.spinner.show()
    this.service.send(this.notificationPayload).subscribe(res => {
      this.spinner.hide()
      if(res.status === 'success') {
        this.toast.success('Push notification sended')
      } else {
        this.toast.error(res.message)
      }
    })
  }

}
